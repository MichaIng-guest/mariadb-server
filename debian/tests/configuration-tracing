#!/bin/sh
# dep8 smoke test for mariadbd
# Author: Otto Kekäläinen <otto@debian.org>
#
# This test should be declared in debian/tests/control with a dependency
# on the package that provides a configured MariaDB server (eg.
# mariadb-server).
#
# This test should be declared in debian/tests/control with the
# following restrictions:
# - needs-root (binaries in /usr/sbin need root to run)
# - allow-stderr (set -x always outputs to stderr, also if mariadbd was not
#   launched as a service it will complain that mysql.plugin table is empty)
#
# This test prints out various configuration information from mariadbd and
# compares the result to expected values in original binary/build.
#

normalize_value() {
  VARIABLE="$1"
  VALUE="$2"
  # In sed the '\s.*' will match whitespace followed by any other chars until end of line
  sed "s/^$VARIABLE\(\s\s*\).*$/$VARIABLE\1$VALUE/" -i "$TEMPFILE"
}

trace() {
  TRACE_NAME="$(echo "$*" | sed -E 's|/usr/(s?)bin/||' | sed 's/ //g' | sed 's/--/-/g')"

  # Show in test what was run
  echo
  echo "Tracing: $*"

  # shellcheck disable=SC2068
  $@ > "$TRACE_NAME.actual"

  # Normalize contents for know special case
  if echo "$*" | grep -q verbose
  then
    TEMPFILE="$(mktemp)"
    # Use 'tail' to skip first line that has version and architecture strings which
    # we intentionally do not want to include in the trace file.
    tail -n +2 "$TRACE_NAME.actual" > "$TEMPFILE"

    # Hostname varies one very machine
    sed "s/$(hostname)/HOSTNAME/g" -i "$TEMPFILE"

    # Version/revision increases on every release
    VERSION=$(mariadbd --help --verbose | grep -e "^version " | rev | cut -d ' ' -f 1 | rev)
    sed "s/$VERSION/VERSION/g" -i "$TEMPFILE"

    # SSL library version inherited form dependency, not relevant for tracing
    # the correctness of the MariaDB build itself
    sed 's/OpenSSL 3.*/SSL-VERSION/' -i "$TEMPFILE"

    # Misc counters and variable values
    normalize_value open-files-limit 32000  # Depends on OS environment
    normalize_value thread-pool-size 2      # Depends on CPU cores available

    mv "$TEMPFILE" "$TRACE_NAME.actual"
  fi

  echo "diff --ignore-space-change -u $TRACE_NAME.expected $TRACE_NAME.actual"
  # Validate that trace file in source code matches tested
  if ! diff --ignore-space-change -u "$TRACE_NAME.expected" "$TRACE_NAME.actual"
  then
    echo "Error: Output from '$*' did NOT match what was expected"
    # Just keep going for debugging purposes now..
    #exit 1
  fi
}

echo "Running test 'configuration-tracing'"
cd debian/tests/traces || exit 1

set -e

# Dump out what parameters mariadb would be called with by default
trace /usr/bin/mariadb --print-defaults

# Dump out all help texts, client variables and their default values
trace /usr/bin/mariadb --verbose --help

# Dump out what parameters mariadbd would be called with by default on system
trace /usr/sbin/mariadbd --print-defaults

# Dump out all help texts, server variables and their default values
trace /usr/sbin/mariadbd --verbose --help
